<?php
/*********************************************************************
 * Test access to the Interswitch API
 *********************************************************************/
require_once('Wrapper.php'); 



# SETTINGS
# Provided by Interswitch for integration
$CLIENT_ID = "your-interswitch-client-id";
$CLIENT_SECRET = "your-interswitch-client-secret";
$TERMINAL_ID = "your-interswitch-terminal-id";
$BANK_CBN_CODE = "your-interswitch-bank-cbn-code";
# Signature settings for access
$SIGNATURE_METHOD = "SHA-256";
$SIGNATURE_TIMEZONE = "Africa/Lagos";



# TESTING
$testWrapper = new Wrapper;
$accessDetails = [
	"client_id"=>$CLIENT_ID,
	"client_secret"=>$CLIENT_SECRET,
	"terminal_id"=>$TERMINAL_ID,
	"bank_cbn_code"=>$BANK_CBN_CODE,
	"signature_method"=>$SIGNATURE_METHOD,
	"signature_timezone"=>$SIGNATURE_TIMEZONE
];


#*********************************************************************
# Get biller categories
$details = array_merge($accessDetails, [
	"url"=>"https://services.interswitchug.com/uatapi/api/v1/quickteller/categorys"
]);
echo "<br>=============================<br>";
echo "<br>Testing get-biller-categories:<br>";
$getBillerCategoriesResponse = $testWrapper->run_get_request($details);
echo json_encode($getBillerCategoriesResponse);


#*********************************************************************
# Validate customer
$data = ["requestReference"=>"test-request-ref", "customerId"=>"test-customer-id", "amount"=>"450", 
	"customerMobile"=>"", "paymentCode"=>"test-payment-code", "bankCbnCode"=>$BANK_CBN_CODE, 
	"terminalId"=>$TERMINAL_ID ];

$details = array_merge($accessDetails, [
	"url"=>"https://services.interswitchug.com/uatapi/api/v1A/svapayments/validateCustomer",
	"data"=>$data
]);

echo "<br>=============================<br>";
echo "<br>Testing validate-customer:<br>";
$validateCustomerResponse = $testWrapper->run_post_request($details);
echo json_encode($validateCustomerResponse);










