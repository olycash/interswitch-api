<?php
/*********************************************************************
 * This class provides a PHP wrapper used in combination with the  
 * Interswitch Java package to make API requests and format responses 
 * for use in a PHP environment.
 *
 * @author Aloysious Zziwa <azziwa@olycash.com>
 * @version 1.0.0
 * @created 02/12/2020
 * @copyright (c) 2020 OlyCash Inc. and its affiliates. 
 *
 * Free permission to use and reproduce granted under MIT license. 
 *********************************************************************/


class Wrapper {

	# Run a GET request on the API
	function run_get_request($parameters) {
		$missing = $this->verify($parameters);
		if(!empty($missing)) return "ERROR: Missing parameters: ".json_encode($missing);

		# There is data to be passed
		if(!empty($parameters['data'])) {
			# TODO: Currently unused
		}

		$command = "GET ".$parameters['url']." ".$parameters['client_id']
			." ".$parameters['client_secret']." ".$parameters['terminal_id']
			." ".$parameters['bank_cbn_code']." ".$parameters['signature_method']
			." ".$parameters['signature_timezone'];

		return $this->run($command);
	}



	# Run a POST request on the API
	function run_post_request($parameters) {
		$missing = $this->verify($parameters);
		if(!empty($missing)) return "ERROR: Missing parameters: ".json_encode($missing);

		# There is data to be passed
		$data = "";
		if(!empty($parameters['data'])) {
			$data  = "\"".addslashes(json_encode($parameters['data']))."\"";
		}

		# There is header fields to be passed
		# NOTE: Order of the parameters for the header fields is IMPORTANT.
		# e.g., amount + terminalId + requestReference + customerId + paymentCode
		$headerFields = "";
		if(!empty($parameters['header_fields'])) {
			foreach($parameters['header_fields'] AS $field=>$value) $headerFields .= $value;
		}
		
		$command = "POST ".$parameters['url']." ".$parameters['client_id']
			." ".$parameters['client_secret']." ".$parameters['terminal_id']
			." ".$parameters['bank_cbn_code']." ".$parameters['signature_method']
			." ".$parameters['signature_timezone'];

		if(!empty($data)) $command .= " ".$data;
		if(!empty($headerFields)) $command .= " ".$headerFields;

		return $this->run($command);
	}




	# Verify passed parameters to ensure that fields required are passed
	function verify($parameters) {
		# These fields are required in the parameters
		$required = ["url", "client_id", "client_secret", "terminal_id", 
			"bank_cbn_code", "signature_method", "signature_timezone"];

		# Ensure all required info is provided
		$missing = array_diff($required, array_keys($parameters));
		if(empty($missing)) {
			foreach($required AS $requiredField) {
				if(empty($parameters[$requiredField])) $missing[] = $requiredField;
			}
		}

		return $missing;
	}




	# Run command on API
	function run($command){
		# WARNING: This function is vulnerable to attack!
		# Ensure that the command string passed is verified before it reaches 
		# this stage or use on a separate expendable server.
		#
		# If the $response is not being received, activate by removing from the
		# "disable_functions" list of functions in your php.ini
		# and your php user (e.g. www-data) can run the Java Request command.
		$response = shell_exec("java -cp ".__DIR__."/lib/ interswitch.Request ".$command);

		if(empty($response) || substr($response, 0,6) == "ERROR:" 
			|| strtolower($response) == "server error") {
			return ["message"=>"error", "details"=>$response];
		}
		else {
			$finalResponse = json_decode($response, TRUE);
			if(json_last_error() != JSON_ERROR_NONE) $finalResponse = $response;

			return ["message"=>"OK", "details"=>$finalResponse ];
		}
	}
}





