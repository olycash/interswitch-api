/********************************************************************
 * This class runs command requests on the Interswitch API.
 *
 * @author Aloysious Zziwa <azziwa@olycash.com>
 * @version 1.0.0
 * @created 02/12/2020
 * @copyright (c) 2020 OlyCash Inc. and its affiliates. 
 *
 * Free permission to use and reproduce granted under MIT license. 
 ********************************************************************/
package interswitch;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader; 
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.BufferedReader;
import java.net.HttpURLConnection;
import java.net.URL; 
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.lang.StringBuilder;

import interswitch.Signature;



public class Request {

	// Called to execute a request on the API
	// Command run in the format:
	// $ java -cp ../ interswitch.Request <parameters>
	//
	// The command parameters are expected IN THE FOLLOWING ORDER as (String)s:
	// [HTTP_METHOD] 		POST or GET
	// [URL]				Full API endpoint URL starting with http...
	// [CLIENT_ID] 			Provided by Interswitch (connection parameters)
	// [CLIENT_SECRET] 		Provided by Interswitch (connection parameters)
	// [TERMINAL_ID] 		Provided by Interswitch (connection parameters)
	// [BANK_CBN_CODE] 		Provided by Interswitch (connection parameters)
	// [SIGNATURE_METHOD]	Method to be used to sign request. 
	//						E.g. SHA-256 (recommended) or SHA-512
	// [SIGNATURE_TIMEZONE] Provided by Interswitch for the signer server. 
	//						Currently, the only valid one is "Africa/Lagos"
	// *[DATA] 				To be used in the request in JSON format.
	// *[HEADER_FIELDS]		Extra fields to be included in the header in 
	///						format "parameter1=value1&parameter2=value2&.."
	//
	// * Are optional parameters

	public static void main (String args[]) 
		throws NoSuchAlgorithmException, IOException {

		// Ensure that the parameters passed are enough and collect the appropriate details
		HashMap<String, String> parameters = new HashMap<String, String>();
		int totalParameters = args.length;

		if(totalParameters > 7){
			parameters.put("HTTP_METHOD", args[0].toUpperCase());
			parameters.put("URL", args[1]);
			parameters.put("CLIENT_ID", args[2]);
			parameters.put("CLIENT_SECRET", args[3]);
			parameters.put("TERMINAL_ID", args[4]);
			parameters.put("BANK_CBN_CODE", args[5]);
			parameters.put("SIGNATURE_METHOD", args[6].toUpperCase());
			parameters.put("SIGNATURE_TIMEZONE", args[7]);

			// Add the optional parameters only if they are provided
			if(args.length > 8) parameters.put("DATA", args[8]);
			else parameters.put("DATA", "");

			if(args.length > 9) parameters.put("HEADER_FIELDS", args[9]);
			else parameters.put("HEADER_FIELDS", "");


			// Now proceed to call appropriate method
			if("GET".equals(parameters.get("HTTP_METHOD"))) runGET(parameters); 
			else if("POST".equals(parameters.get("HTTP_METHOD"))) runPOST(parameters);
			else System.out.println("ERROR: HTTP method not supported.");

		} else {
			System.out.println("ERROR: Invalid request details. See class documentation.");
		}
	}








	// Run a GET request on the API
	public static void runGET(HashMap<String, String> parameters) 
		throws NoSuchAlgorithmException, IOException  {

		// First get the signature for our request
		HashMap<String, String> runSignature = Signature.generate(parameters.get("HTTP_METHOD"), 
				parameters.get("URL"), parameters.get("CLIENT_ID"), parameters.get("CLIENT_SECRET"), 
				parameters.get("SIGNATURE_METHOD"), parameters.get("SIGNATURE_TIMEZONE"), 
				parameters.get("HEADER_FIELDS") );

		// Then run on the API to get the response
		URL urlObject = new URL(parameters.get("URL"));
		HttpURLConnection connection = (HttpURLConnection) urlObject.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		connection.setRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Authorization", runSignature.get("AUTHORIZATION"));
		connection.setRequestProperty("Timestamp", runSignature.get("TIMESTAMP"));
		connection.setRequestProperty("Nonce", runSignature.get("NONCE"));
		connection.setRequestProperty("Signature", runSignature.get("SIGNATURE"));
		connection.setRequestProperty("SignatureMethod", runSignature.get("SIGNATURE_METHOD"));
		connection.setRequestProperty("TerminalId", parameters.get("TERMINAL_ID"));

		// Process response
		int responseCode = connection.getResponseCode();

		// Use this to check API response HTTP code
		// System.out.println("API Response Code: " + responseCode);

		InputStream inputStream = connection.getInputStream();
		StringBuffer response = new StringBuffer();
		int c;
		while ((c = inputStream.read()) != -1) response.append((char) c);

		// Printout response
		System.out.println(response);
	}










	// Run a POST request on the API
	public static void runPOST(HashMap<String, String> parameters) 
		throws NoSuchAlgorithmException, IOException  {

		// Enforce having data to POST before proceeding
		if("".equals(parameters.get("DATA"))) {
			System.out.println("ERROR: Data to post to API is required to continue.");
			return;
		}
		
		// But ofcourse, first get the signature for our request
		HashMap<String, String> runSignature = Signature.generate(parameters.get("HTTP_METHOD"), 
				parameters.get("URL"), parameters.get("CLIENT_ID"), parameters.get("CLIENT_SECRET"), 
				parameters.get("SIGNATURE_METHOD"), parameters.get("SIGNATURE_TIMEZONE"), 
				parameters.get("HEADER_FIELDS") );

		// Then run on the API to get the response
		URL urlObject = new URL(parameters.get("URL"));
		HttpURLConnection connection = (HttpURLConnection) urlObject.openConnection();
		connection.setDoOutput(true);
		connection.setRequestMethod("POST"); 
		connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		connection.setRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Authorization", runSignature.get("AUTHORIZATION"));
		connection.setRequestProperty("Timestamp", runSignature.get("TIMESTAMP"));
		connection.setRequestProperty("Nonce", runSignature.get("NONCE"));
		connection.setRequestProperty("Signature", runSignature.get("SIGNATURE"));
		connection.setRequestProperty("SignatureMethod", runSignature.get("SIGNATURE_METHOD"));
		connection.setRequestProperty("TerminalId", parameters.get("TERMINAL_ID"));

		// Attempt posting our request
		try(OutputStream stream = connection.getOutputStream()) {
			byte[] input = parameters.get("DATA").getBytes("utf-8");
			stream.write(input, 0, input.length);           
		}


		// Process response  
		int responseCode = connection.getResponseCode(); 

		// Use this to check API response HTTP code
		// System.out.println("API Response Code: " + responseCode);

		StringBuilder builder = new StringBuilder();

		// Post was a success (according to responseCode)
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
			String line = null;  
			while ((line = reader.readLine()) != null) builder.append(line + "\n");  
			reader.close();
			System.out.println("" + builder.toString());  
		} 
		// Oh no! an error occurred.
		else {
			System.out.println("ERROR: Details..");
			System.out.println(connection.getResponseMessage());  
		}  
	}

}







