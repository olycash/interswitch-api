/********************************************************************
 * This class signs user access to the Interswitch API.
 *
 * @author Aloysious Zziwa <azziwa@olycash.com>
 * @version 1.0.0
 * @created 02/12/2020
 * @copyright (c) 2020 OlyCash Inc. and its affiliates. 
 *
 * Free permission to use and reproduce granted under MIT license. 
 ********************************************************************/
package interswitch;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.UUID;
import java.util.Base64;



public class Signature {

	public static HashMap<String, String> generate(
			String httpMethod, String resourceUrl, String clientId,
			String clientSecretKey, String signatureMethod, 
			String timeZone, String additionalParameters) 
		throws UnsupportedEncodingException, NoSuchAlgorithmException {

		// TIMESTAMP: Set timestamp in signer timezone location
		TimeZone signerTimeZone = TimeZone.getTimeZone(timeZone);
		Calendar calendar = Calendar.getInstance(signerTimeZone);
		// Timestamp must be in seconds
		long timestamp = calendar.getTimeInMillis() / 1000;

		// NONCE: Generate nonce for signature
		UUID uuid = UUID.randomUUID();
		String nonce = uuid.toString().replaceAll("-", "");

		// AUTHORIZATION: Get client ID as base64
		String clientIdBase64 = new String(Base64.getEncoder().encode(clientId.getBytes()));
		
		// Prepare encoded URL for use in signature
		String encodedResourceUrl = URLEncoder.encode(resourceUrl, "ISO-8859-1");
		String signatureCipher = httpMethod + "&" + encodedResourceUrl + "&"
				+ timestamp + "&" + nonce + "&" + clientId + "&" + clientSecretKey;

		// Include additional parameters to URL if passed
		if (additionalParameters != null && !"".equals(additionalParameters)) {
			signatureCipher = signatureCipher + "&" + additionalParameters;
		}

		// SIGNATURE: Form signature as cipher
		MessageDigest messageDigest = MessageDigest.getInstance(signatureMethod);
		byte[] signatureBytes = messageDigest.digest(signatureCipher.getBytes());
		// Encode signature as base 64 
		String signature = new String(Base64.getEncoder().encode(signatureBytes));

		// Prepare response
		HashMap<String, String> response = new HashMap<String, String>();
		response.put("TIMESTAMP", String.valueOf(timestamp));
		response.put("NONCE", nonce);
		response.put("AUTHORIZATION", "InterswitchAuth " + clientIdBase64);
		response.put("SIGNATURE", signature);
		response.put("SIGNATURE_METHOD", signatureMethod);

		return response;
	}
}




