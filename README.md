# interswitch-api

ABOUT:
Use to run requests on the Interswitch (interswitchgroup.com) API
with either Java or a PHP wrapper.

This software was initially provided by OlyCash (olycash.com) to 
encourage software solutions for the unbanked.


CONTRIBUTING:
Feel free to submit merge requests for bugfixes and improvements. 


LICENSE:
This project code is provided free to use and reproduce under MIT license.


